import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { SplashScreen} from "@ionic-native/splash-screen";
import { StatusBar} from "@ionic-native/status-bar";

import { AuthServiceProvider } from '../providers/auth-service/auth-service';
//import { EventProvider } from '../providers/kostizi/event-provider';
import { SmartAudio } from '../providers/smart-audio/smart-audio';

import { ConfigPage } from '../pages/config/config';
import { ScanPage } from '../pages/scan/scan';

@Component({
  templateUrl: 'app.html'
})
export class KostiziScan {

  @ViewChild(Nav) nav: Nav;
  rootPage: any;
  config: any;
  pages: Array<{title: string, component: any, icon: string}>;

  constructor(
    public platform: Platform,
    public splashScreen: SplashScreen,
    public statusBar: StatusBar,
    public authService: AuthServiceProvider,
    public smartAudio: SmartAudio
  ) {

    let env = this;

    this.pages = [
      { title: 'Configuration', component: ConfigPage, icon: 'cog' },
      { title: 'Scan', component: ScanPage, icon: 'barcode' }
    ];

    this.platform.ready().then(() => {

      env.splashScreen.hide();
      env.statusBar.styleDefault();

      env.smartAudio.preload('valid','assets/audio/valid.wav');
      env.smartAudio.preload('invalid','assets/audio/invalid.wav');
      env.smartAudio.preload('choose','assets/audio/attention.wav');
      env.smartAudio.preload('duplicate','assets/audio/duplicate.wav');

      env.authService.getConfig().then((config) => {

        env.config = config;

        if (env.config.configured) {
          env.nav.setRoot(ScanPage);
        } else {
          env.nav.setRoot(ConfigPage);
        }

      });

    });

  }

  /* navigate to selected page */
  goToPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  /* exit from the app */
  doExit() {
    this.platform.exitApp();
  }

}
