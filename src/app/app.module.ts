import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { HttpModule } from '@angular/http';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeAudio } from '@ionic-native/native-audio';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Device } from '@ionic-native/device';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Keyboard } from '@ionic-native/keyboard';

import { KostiziScan } from './app.component';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { EventProvider } from '../providers/kostizi/event-provider';
import { RedemptionProvider } from '../providers/kostizi/redemption-provider';
import { SmartAudio } from '../providers/smart-audio/smart-audio';
import { LocationProvider } from '../providers/location/location-provider';

import { ConfigPage } from '../pages/config/config';
import { ChoosePage } from '../pages/choose/choose';
import { ScanPage } from '../pages/scan/scan';

@NgModule({
  declarations: [
    KostiziScan,
    ConfigPage,
    ScanPage,
    ChoosePage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(KostiziScan),
    IonicStorageModule.forRoot({
      name: '__mydb',
       driverOrder: ['indexeddb', 'sqlite', 'websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    KostiziScan,
    ConfigPage,
    ScanPage,
    ChoosePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Keyboard,
    Storage,
    Geolocation,
    AuthServiceProvider,
    EventProvider,
    RedemptionProvider,
    LocationProvider,
    BarcodeScanner,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    NativeAudio,
    SmartAudio,
    Device
  ]
})
export class AppModule {}
