import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the ConsumerProvider provider.
  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/

@Injectable()

export class EventProvider {

  data: any[];

  constructor(private http: Http) {  }


  getEvents(params) {

    let env = this;

    return new Promise((resolve, reject) => {
      var apiURL = 'https://www.textizi.com/ajaxTickets/getMerchantEvents?testtoken=quark';

      let headers = new Headers();
      headers.append('Content-Type','application/json');

      this.http.post(apiURL, JSON.stringify(params), {headers:headers})
        .map(res => res.json())
        .subscribe(
          result => {
            if (result.error === undefined) {
              reject("Error communicating with Kostizi server.");
            } else if (result.error === 1) {
              reject(result.message);
            } else {
              env.data = result.qevents.data;
              resolve(env.data);
            }
          },
          err => {
            console.log(err);
            reject(err);
          }
      );

    });
  }

}
