import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the ConsumerProvider provider.
  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/

@Injectable()

export class RedemptionProvider {

  data: any[];

  constructor(private http: Http) {
    this.data = null;
  }

  redeem(params) {

    // If we have a numeric eventId, restrict the validation to that specific event.
    // Otherwise, allow all events for the given merchantId.
    params.thisEventOnly = true;
    if (isNaN(params.eventId)) {
      params.thisEventOnly = false;
    }

    return new Promise((resolve, reject) => {
      let apiURL = 'https://www.textizi.com/api/merchant/ticket/redeem?testtoken=quark';

      let headers = new Headers();
      headers.append('Content-Type','application/json');

      this.http.post(apiURL, JSON.stringify(params), {headers:headers})
        .map(res => res.json())
        .subscribe(
          result => {
            if (result['ERROR'] === undefined) {
              this.data = null;
              reject("Error communicating with Kostizi server.");
            } else if (result['ERROR'] === 1) {
              this.data = null;
              reject(result['MESSAGE']);
            } else {
              this.data = result;
              resolve(result);
            }
          },
          err => {
            console.log(err);
            reject(err);
          }
      );

    });
  }

  redeemSelection(params) {

    return new Promise((resolve, reject) => {
      let apiURL = 'https://www.textizi.com/api/merchant/ticket/redeemSelection?testtoken=quark';

      let headers = new Headers();
      headers.append('Content-Type','application/json');

      this.http.post(apiURL, JSON.stringify(params), {headers:headers})
        .map(res => res.json())
        .subscribe(
          result => {
            this.data = result;
            resolve(result);
          },
          err => {
            console.log(err);
            reject(err);
          }
      );

    });
  }

}
