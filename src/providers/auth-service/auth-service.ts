import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
import { Device } from '@ionic-native/device';
import 'rxjs/add/operator/map';

/*
  Generated class for the AuthServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class AuthServiceProvider {

  private config: any;
  private loaded: boolean = false;

  constructor(
    public http: Http,
    public storage: Storage,
    public device: Device
  ) {

    this.config = this.getBlankConfig();
    this.loaded = false;

  }


  getConfig() {

    let env = this;

    return new Promise((resolve, reject) => {

      if (env.loaded) {

        resolve(env.config);

      } else {

        // Try to get config data from device storage.
        env.storage.get('config').then((config) => {

          if (!config) {

            env.config = env.getBlankConfig();
            env.loaded = true;

          } else {

            env.config = config;
            env.config.deviceId = env.device.uuid;
            env.loaded = true;

          }

          resolve(env.config);

        })
        .catch((err) => {
          env.config = env.getBlankConfig();
          console.log("Encountered error while executing env.storage.get:")
          console.log(err);
          reject(err);
        });

      }

    });

  }


  setConfig(args) {

    let env = this;

    let params = {
      token: args.token,
      latitude: args.latitude,
      longitude: args.longitude,
      isPrinter: false,
      deviceId: this.device.uuid
    };

    return new Promise((resolve, reject) => {
      let apiURL = 'https://www.textizi.com/ajaxRedeem/validateEventToken?testtoken=quark';

      let headers = new Headers();
      headers.append('Content-Type','application/json');

      env.http.post(apiURL, JSON.stringify(params), {headers:headers})
        .map(res => res.json())
        .subscribe(
          result => {
            if (!result.isvalid) {
              env.config = env.getBlankConfig();
              env.storage.remove('config').then(() => {
                reject("Token is not valid.");
              });
            } else {
              //env.config.merchantId = 96;
              //env.config.merchanntLocationId = 68;
              env.config.deviceId = env.device.uuid;
              env.config.token = args.token;
              env.config.latitude = args.latitude;
              env.config.longitude = args.longitude;
              env.config.merchantId = result.qdata.data[0].merchantid;
              env.config.eventId = result.qdata.data[0].eventid;
              env.config.merchantName = result.qdata.data[0].merchantname;
              env.config.eventTitle = result.qdata.data[0].eventtitle;
              env.config.configured = true;
              env.storage.set('config', env.config).then(() => {
                resolve(env.config);
              });
            }
          },
          err => {
            console.log(err);
            reject(err);
          }
      );

    });

  }


  getBlankConfig() {

    return {
      merchantId: 0,
      merchantName: 0,
      eventId: 0,
      eventTitle: 0,
      token: "",
      deviceId: this.device.uuid,
      latitude: "",
      longitude: "",
      doAutofocus: false,
      maxItems: 20,
      configured: false
    };

  }

}
