import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import 'rxjs/add/operator/map';


/*
  Generated class for the LocationProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class LocationProvider {

  data: any;
  doRefresh: boolean;

  constructor(
    private geolocation: Geolocation
  ) {
    this.data = null;
  }

  getLocation(doRefresh) {

    if (doRefresh == true) {
      this.data = null;
      doRefresh = false;
    }

    if (this.data) {
      return Promise.resolve(this.data);
    }

    return new Promise((resolve, reject) => {

      this.geolocation.getCurrentPosition().then((position) => {
        this.data = position;
        resolve(this.data);
      }, (err) => {
        console.log("Location acquisition failed:");
        console.log(err);
        this.data = {
          coords:{
            latitude:'',
            longitude:''
          }
        }
        resolve(this.data);
      });

    });
  }

}
