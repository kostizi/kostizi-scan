import { Component } from '@angular/core';
import { AlertController, NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';

import { RedemptionProvider } from '../../providers/kostizi/redemption-provider';
import { SmartAudio } from '../../providers/smart-audio/smart-audio';

@Component({
  selector: 'page-choose',
  templateUrl: 'choose.html'
})
export class ChoosePage {

  public redeemOptions: any[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public redemptionProvider: RedemptionProvider,
    public smartAudio: SmartAudio
  ) {
    this.redeemOptions = [];
  }

  ionViewDidLoad() {
    this.redeemOptions = this.navParams.get('redeemOptions');
  }


  redeem(redeemOption,i) {

    let env = this;

    if ((redeemOption['TIME'] != null) && redeemOption['TIME'].length > 0) {

      return;

    } else {

      let loading = this.loadingCtrl.create({
        content: "Working..."
      });

      loading.present();

      let params = {
        "redemptionId" : redeemOption['REDEMPTIONID']
      };

      env.redemptionProvider.redeemSelection(params).then((response:string) => {

        if (response.length > 0) {

          // play the valid sound
          env.smartAudio.play('valid');

          // remove the just-redeemed item
  				env.redeemOptions.splice(i,1);

  				// visually mark this as redeemed
  				redeemOption['TIME'] = response;
  				redeemOption['SECONDSAGO'] = 0;

          // insert the just-redeemed item at the bottom
  				env.redeemOptions.push(redeemOption);

        } else {

          // play the valid sound
          env.smartAudio.play('invalid');

        }

/*
        // add this redemption to the redeem waterfall
				var data = {
					REDEMPTIONVALUE: redeemOption.REDEMPTIONVALUE,
					STATUS: redeemOption.STATUS,
					CONSUMERNAME: redeemOption.CONSUMERNAME,
					MESSAGE: redeemOption.MESSAGE,
					TIME: redeemOption.TIME,
					SECONDSAGO: ''
				};

				addRedemption(data);
*/

        loading.dismiss();

      }).catch((err) => {
        console.log(err);
        let alert = env.alertCtrl.create({
          title: 'Redemption failed.',
          subTitle: err,
          buttons: ['OK']
        });
        loading.dismiss();
        alert.present();
      });

    }

  }


  parseSeconds(seconds) {
		let timeElapsedString = '';

		if (seconds < 0) {
			return timeElapsedString;
		}

		var minutes = Math.trunc(seconds / 60);
		var hours = Math.trunc(minutes / 60);

		if (hours > 0) {
			timeElapsedString = hours + ' hour(s) ago';
		} else if (minutes > 0) {
			timeElapsedString = minutes + ' minute(s) ago';
		} else {
			timeElapsedString = seconds + ' second(s) ago';
		}

		return timeElapsedString;
	}


  dismissModal() {
    this.viewCtrl.dismiss();
  }

}
