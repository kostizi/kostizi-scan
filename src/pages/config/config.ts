import { Component } from '@angular/core';
import { AlertController, NavController, NavParams } from 'ionic-angular';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { Device } from '@ionic-native/device';

import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { LocationProvider } from '../../providers/location/location-provider';

import { ScanPage } from '../../pages/scan/scan';
/**
 * Generated class for the ConfigurePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-config',
  templateUrl: 'config.html',
})
export class ConfigPage {

  private config: any;
  private scanOptions: BarcodeScannerOptions;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public authService: AuthServiceProvider,
    public locationProvider: LocationProvider,
    public barcodeScanner: BarcodeScanner,
    public device: Device
  ) {
    this.config = authService.getBlankConfig();
  }

  ionViewDidLoad() {
    let env = this;

    this.authService.getConfig().then((config) => {

      env.config = config;

      if (!env.config.configured) {

        // No config data yet. Get this device's location if we can.
        env.locationProvider.getLocation(true).then((position) => {
          env.config.latitude = position.coords.latitude;
          env.config.longitude = position.coords.longitude;
        });

      }

    });

  }


  scan() {
    let env = this;

    env.scanOptions = {
      prompt : "Scan your QR code."
    }
    env.barcodeScanner.scan(env.scanOptions).then((barcodeData) => {
      if (!barcodeData.cancelled) {
        env.config.token = barcodeData.text;
      }
    }, (err) => {
      console.log(err);
      let alert = env.alertCtrl.create({
        title: 'Scan failed.',
        subTitle: err,
        buttons: ['OK']
      });
      alert.present();
    });
  }


  setConfig() {
    let env = this;

    env.authService.setConfig(env.config).then((config) => {
      env.navCtrl.setRoot(ScanPage);
    }).catch((err) => {
      let alert = env.alertCtrl.create({
        title: 'Configuration failed.',
        subTitle: err,
        buttons: ['OK']
      });
      alert.present();
    });
  }


}
