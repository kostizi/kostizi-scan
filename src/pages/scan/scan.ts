import { Component, ViewChild } from '@angular/core';
import { AlertController, NavController, NavParams, LoadingController, ModalController } from 'ionic-angular';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { Keyboard } from '@ionic-native/keyboard';

import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { EventProvider } from '../../providers/kostizi/event-provider';
import { RedemptionProvider } from '../../providers/kostizi/redemption-provider';
import { SmartAudio } from '../../providers/smart-audio/smart-audio';

import { ConfigPage } from '../../pages/config/config';
import { ChoosePage } from '../../pages/choose/choose';

@Component({
  selector: 'page-scan',
  templateUrl: 'scan.html'
})
export class ScanPage {

  public serialNumber: any;
  //public events: any[];
  //public selectedEvent: any;
  public redeemOptions: any[];
  public redemptions: any[];
  public lastRedemptionValue: any;
  private config: any;
  private scanOptions: BarcodeScannerOptions;

  //@ViewChild('snf') snf: ElementRef;
  @ViewChild('snf') snf;

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public authService: AuthServiceProvider,
    public eventProvider: EventProvider,
    public redemptionProvider: RedemptionProvider,
    public smartAudio: SmartAudio,
    public barcodeScanner: BarcodeScanner,
    public keyboard: Keyboard
  ) {

    this.config = authService.getBlankConfig();
    this.serialNumber = '';
    this.redeemOptions = [];
    this.redemptions = [];
    this.lastRedemptionValue = '';

  }


  ionViewDidLoad() {
    console.log("ionViewDidLoad...");

    let env = this;

    let loading = this.loadingCtrl.create({
      content: "Working..."
    });

    loading.present();

    this.authService.getConfig().then((config) => {

      env.config = config;

      if (env.config.configured) {

        /*
        Trying to keep the keyboard hidden even when the form field is in focus.
        This doesn't work, because closing the keyboard removes focus from the field.
        if (env.config.doAutofocus) {
          console.log("Subscribing to onKeyboardShow...");
          env.keyboard.onKeyboardShow().subscribe(e => {
            setTimeout(() => {
              console.log("Closing keyboard...");
              env.keyboard.close();
            }, 50);
          });
        }
        */

        loading.dismiss();

        if (env.config.doAutofocus) {
          setTimeout(() => {
            env.snf.setFocus();
          },600);
        }

      } else {

        loading.dismiss();
        env.navCtrl.setRoot(ConfigPage);

      }

    });

  }


  scan() {
    let env = this;

    env.scanOptions = {
      prompt : "Scan barcode."
    }
    env.barcodeScanner.scan(env.scanOptions).then((barcodeData) => {
      if (!barcodeData.cancelled) {
        env.serialNumber = barcodeData.text;
        env.redeem();
      }
    }, (err) => {
      console.log(err);
      let alert = env.alertCtrl.create({
        title: 'Scan failed.',
        subTitle: err,
        buttons: ['OK']
      });
      alert.present();
    });
  }


  redeem() {

    let env = this;

    env.serialNumber = env.serialNumber.trim();

    if (env.serialNumber.length === 0) {

      env.serialNumber = '';
      return;

    } else if (env.serialNumber == env.lastRedemptionValue) {

      env.serialNumber = '';
      env.smartAudio.play('duplicate');
      return;

    } else {

      env.redeemOptions = [];
      env.lastRedemptionValue = env.serialNumber;

      let loading = this.loadingCtrl.create({
        content: "Working..."
      });

      loading.present();

      let params = {
        "merchantId" : env.config.merchantId,
        "eventId" : env.config.eventId,
        "redemptionValue" : env.serialNumber
      };

      env.redemptionProvider.redeem(params).then((response) => {

        if (response['ERROR'] == true) {
          env.smartAudio.play('invalid');
          let alert = env.alertCtrl.create({
            title: 'Redemption failed.',
            subTitle: response['MESSAGE'],
            buttons: ['OK']
          });
          loading.dismiss();
          alert.present();
          return;
				}

				switch(response['STATUS']){

					case 'REDEEMED':
						env.smartAudio.play('valid');
						break;

					case 'INVALID':
            env.smartAudio.play('invalid');
						break;

					case 'CHOOSE':
            env.smartAudio.play('choose');
						break;
				}

        env.serialNumber = '';

        if (response['STATUS'] === 'CHOOSE') {

          let modal = this.modalCtrl.create(ChoosePage,{
            redeemOptions: response['REDEEMOPTIONS']
          });
          loading.dismiss();
          modal.present();

        } else {

  				// add this redemption to the waterfall array
  				env.addRedemption(response);
          loading.dismiss();

        }

      }).catch((err) => {
        console.log(err);
        let alert = env.alertCtrl.create({
          title: 'Redemption failed.',
          subTitle: err,
          buttons: ['OK']
        });
        loading.dismiss();
        alert.present();
      });

    }

  }


  parseSeconds(data) {
		let timeElapsedString = '';
    let seconds = data['SECONDSAGO']

		if (seconds < 0) {
			return timeElapsedString;
		} else if (seconds == 0) {
      return "just now";
    }

		let minutes = Math.trunc(seconds / 60);
		let hours = Math.trunc(minutes / 60);
    let days = Math.trunc(hours / 24);

		if (days == 1) {
      timeElapsedString = 'yesterday';
		} else if (days > 1) {
      timeElapsedString = days + ' days ago';
    } else if (hours == 1) {
      timeElapsedString = 'an hour ago';
    } else if (hours > 1) {
      timeElapsedString = hours + ' hours ago';
    } else if (minutes == 1) {
			timeElapsedString = 'a minute ago';
    } else if (minutes > 0) {
			timeElapsedString = minutes + ' minutes ago';
    } else if (seconds == 1) {
			timeElapsedString = 'a second ago';
    } else {
			timeElapsedString = seconds + ' seconds ago';
		}

		return timeElapsedString;
	}


	// add redemption to the waterfall
	addRedemption(redemption){

		// put the data for the new redemption into the array
		this.redemptions.push(redemption);

		// if we have reached our set amount of redemptions to show, splice the array
		if (this.redemptions.length > this.config.maxItems) {
      this.redemptions.splice(0,1);
		}

	}


  // If auto-focus is enabled, put the focus back on the input field.
  focusCheck(event) {

    let env = this;

    if (env.config.doAutofocus) {
      setTimeout(() => {
        env.snf.setFocus();
      },500);
    }

  }

  /*
  suppressKeyboard(event) {

    if (this.config.doAutofocus) {
      this.keyboard.close();
      event.preventDefault();
      event.stopImmediatePropagation();
    }

  }
  */

}
